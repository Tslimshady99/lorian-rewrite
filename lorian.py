import discord
from discord.ext import commands

from os import listdir
from os.path import isfile, join
from imports import *
import sys, traceback
import yaml

cogs_dir = "cogs"

with open("config.yml", 'r') as f:
    config = yaml.load(f)

def get_prefix(bot, message):
    prefixes = ['??', 'l?']
    return commands.when_mentioned_or(*prefixes)(bot,message)

bot = commands.Bot(command_prefix=get_prefix, description='Lorian')

if __name__ == '__main__':
    for extension in [f.replace('.py', '') for f in listdir(cogs_dir) if isfile(join(cogs_dir, f))]:
        try:
            bot.load_extension(cogs_dir + "." + extension)
        except (discord.ClientException, ModuleNotFoundError):
            print(f'Failed to load extension {extension}.')
            print(traceback.print_exc())

@bot.event
async def on_ready():
    print(f'\n\nLogged in as: {bot.user.name} - {bot.user.id}\nVersion: {discord.__version__}\n')
    print(f'Successfully logged in and booted...!')


bot.run(config.get('token'), bot=True, reconnect=True)
